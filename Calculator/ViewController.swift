//
//  ViewController.swift
//  Calculator
//
//  Created by Koniushenko, Andrii on 05/11/16.
//  Copyright © 2016 Koniushenko, Andrii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var display: UILabel!
    
    @IBOutlet private weak var typingHistory: UILabel!
    
    private var userIsInTheMiddleOfTyping = false

    private var commandIsStartTyping = false

    @IBAction private func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = display.text!
            display.text = textCurrentlyInDisplay + digit
        } else {
            display.text = digit
            userIsInTheMiddleOfTyping = true
        }
    }
    
    private var displayValue: Double {
        get{
            return Double(display.text!)!
        }
        set{
            display.text = String(newValue)
        }
    }
    
    private var typingCommand: String {
        get{
            return typingHistory.text!
        }
        set{
            typingHistory.text = newValue
        }
    }
    
    private var brain = CalculatorBrain()
    
    @IBAction private func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            brain.setOperand(operand: displayValue)
            userIsInTheMiddleOfTyping = false
        }
        
        if let mathematicalSymbol = sender.currentTitle {
           brain.performOperation(symbol: mathematicalSymbol)
           printCommands(mathematicalSymbol)
        }
        
        display.text = String(brain.result)
        
    }
    
    private func printCommands(_ mathematicalSymbol: String){
        if commandIsStartTyping {
            typingHistory.text = String(format: "%@ %@", typingCommand, mathematicalSymbol)
        } else {
            typingHistory.text = mathematicalSymbol
            commandIsStartTyping = true
        }
    }
    
}

